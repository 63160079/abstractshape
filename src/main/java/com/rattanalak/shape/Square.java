/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.shape;

/**
 *
 * @author Rattanalak
 */
public class Square extends Shape {
    private double side;

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        if (side <= 0) {
            System.out.println("Error: side must more than Zero!!!");
            return;
        }
        this.side = side;
    }

    public Square(double side) {
        super("Square");
        this.side = side;
    }

    @Override
    public double calArea() {
        return side * side;
    }

    @Override
    public String toString() {
        return "Square{" + "side=" + side + '}';
    }

}
